export const chartData = [
    {
      name: 'Jan',
      "Active user": 4000,
    },
    {
      name: 'Feb',
      "Active user": 3000,
    },
    {
      name: 'Mar',
      "Active user": 2000,
    },
    {
      name: 'Apr',
      "Active user": 2780,
    },
    {
      name: 'May',
      "Active user": 1890,
    },
    {
      name: 'Jun',
      "Active user": 2390,
    },
    {
      name: 'Jul',
      "Active user": 3490,
    },
    {
      name: 'Aug',
      "Active user": 3000,
    },
    {
      name: 'Sep',
      "Active user": 2000,
    },
    {
      name: 'Oct',
      "Active user": 2780,
    },
    {
      name: 'Nov',
      "Active user": 3000,
    },
    {
      name: 'Dec',
      "Active user": 2000,
    },
  ];