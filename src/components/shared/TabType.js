export const TAB_TYPE = {
    USERS: "/users",
    USER: "/user",
    NEW_USER: "/newuser",
    PRODUCTS: "/products",
    PRODUCT: "/product",
    NEW_PRODUCT: "/newproduct",
//+++++++++++++++++++++++++++++++++++++++
    MY_DRAFTS: "/myDrafts",

    DASHBOARD: "/dashboard",
    MY_DOCS: "/myDocs",
    NEW_DOC: "/newDoc",
    VIEW_DOC: "/viewDoc",
    EDIT_DOC: "/editDoc",
    
}
