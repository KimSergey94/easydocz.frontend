import './widgetLg.css'

export default function widgetlg() {
    const Button = ({type}) => {
        return <button className={"widgetLgButton " + type}>{type}</button>
    }
    return (
        <div className="widgetLg">
            <h3 className="widgetLgTitle">Последние транзакции</h3>
            <table className="widgetLgTable">
            <thead>
                <tr className="widgetLgTr">
                    <th className="widgetLgTh">Клиент</th>
                    <th className="widgetLgTh">Дата</th>
                    <th className="widgetLgTh">Сумма</th>
                    <th className="widgetLgTh">Статус</th>
                </tr>
            </thead>
                <tbody>
                    <tr className="widgetLgTr">
                        <td className="widgetLgUser">
                            <img src="" alt="" className="widgetLgImg" />
                            <span className="widgetLgName">Susan Carol</span>
                        </td>
                        <td className="widgetLgDate">2 Jun 2021</td>
                        <td className="widgetLgAmount">$122.00</td>
                        <td className="widgetLgStatus"><Button type="Approved"/></td>
                    </tr>
                    <tr className="widgetLgTr">
                        <td className="widgetLgUser">
                            <img src="" alt="" className="widgetLgImg" />
                            <span className="widgetLgName">Susan Carol</span>
                        </td>
                        <td className="widgetLgDate">2 Jun 2021</td>
                        <td className="widgetLgAmount">$122.00</td>
                        <td className="widgetLgStatus"><Button type="Approved"/></td>
                    </tr>
                    <tr className="widgetLgTr">
                        <td className="widgetLgUser">
                            <img src="" alt="" className="widgetLgImg" />
                            <span className="widgetLgName">Susan Carol</span>
                        </td>
                        <td className="widgetLgDate">2 Jun 2021</td>
                        <td className="widgetLgAmount">$122.00</td>
                        <td className="widgetLgStatus"><Button type="Pending"/></td>
                    </tr>
                    <tr className="widgetLgTr">
                        <td className="widgetLgUser">
                            <img src="" alt="" className="widgetLgImg" />
                            <span className="widgetLgName">Susan Carol</span>
                        </td>
                        <td className="widgetLgDate">2 Jun 2021</td>
                        <td className="widgetLgAmount">$122.00</td>
                        <td className="widgetLgStatus"><Button type="Declined"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
