import React from 'react'
import './topbar.css'
import { NotificationsNone, Language, Settings } from '@material-ui/icons'
import Button from '@material-ui/core/Button';
import NoteAddTwoToneIcon from '@material-ui/icons/NoteAddTwoTone';
import {Link} from 'react-router-dom'

export default function Topbar() {
    return (
        <div className="topbarContainer">
            <div className="topbar">
                <div className="topbarWrapper">
                    <div className="topLeft">
                        <span className="logo"><img className="topbarTopLeftLogo" src="/assets/images/docz_logo_5.png" alt="Easy Docs" /></span>
                        <div className="topbarMiddleIconContainer">
                            <Link to="/newdoc" style={{ textDecoration: 'none' }}>
                                <Button color="primary">
                                    <NoteAddTwoToneIcon />
                                    Создать документ
                                </Button>
                            </Link>
                        </div>
                    </div>
                    <div className="topRight">
                       
                        <div className="topbarIconContainersContainer">
                            <div className="topbarIconContainer">
                                <NotificationsNone className="topbarIcon" />
                                <span className="topIconBadge">2</span>
                            </div>
                            <div className="topbarIconContainer">
                                <Language className="topbarIcon" />
                                <span className="topIconBadge">3</span>
                            </div>
                            <div className="topbarIconContainer">
                                <Settings style={{width:'32px',height:'32px'}} />
                                <span className="topIconBadge">4</span>
                            </div>
                        </div>
                        <div className="topbarProfile">
                            <img src="/assets/images/img_avatar.png" alt="Profile" className="topAvatar" />
                            <span className="topbarProfileName">
                                Mikhail Prokhorov                        
                            </span>
                        </div>
                    </div>
                </div>
                <div className="links-container"></div>
            </div>
        </div>
        
    )
}
