import './newDocContainer.css'
import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { FormGroup } from '@material-ui/core';


import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import { useTheme } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
// import Moment from 'react-moment';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      }
    },
    formControl: {
        //margin: theme.spacing(1),
        minWidth: 500,
        maxWidth: '99%',
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
  }));


  const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  
  const names = [
    'Айдар Ахметкалиевич',
    'Mikhail Prokhorov',
    'Ким Сергей Дмитриевич',
    'Альжанова Эльвира Бауыржановна',
    'Алькенова Дана Талгатовна',
    'Аскарова Алия Кудайбергеновна',
    'Ахметова Айгерим Максатовна',
    'Баймаганбетов Нурбол Байболатович',
    'Башчибаев Ахметкали Мухамедгалиевич',
    'Бектембаев Кайыржан Ардакович',
    'Бельгибаева Айдана',
    'Бокаева Гульдана',
    'Глазунова Екатерина Александровна',
    'Есенова Бахытжан Амантаевна',
    'Жаксыбаев Алмас Аскарович',
  ];
  function getStyles(name, personName, theme) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }
  var date = moment(new Date()).format('DD.MM.YYYY hh:mm');

export default function NewDocContainer() {
    const classes = useStyles();

    const [approvalsName, setApprovalsName] = React.useState([]);
    const [signersName, setSignersName] = React.useState([]);
    const [receiversName, setReceiversName] = React.useState([]);
    const handleApprovalsChange = (event) => {
        setApprovalsName(event.target.value);
    };
    const handleSignersChange = (event) => {
      setSignersName(event.target.value);
    };
    const handleReceiversChange = (event) => {
        setReceiversName(event.target.value);
    };
    const theme = useTheme();


    return (
        <div className="newDocContainer">
            <h4 className="newDocSectionTitle">Общее</h4>
            <form className={classes.root + " newDocForm"} noValidate autoComplete="off">
                <FormGroup className="newDocFormGroupGeneral">
                    <FormControl variant="filled" disabled className="newDocFormControl">
                        <InputLabel htmlFor="doc.created">Создан</InputLabel>
                        <FilledInput id="doc.created" style={{backgroundColor:'rgb(97 131 175 / 33%)'}} value={date} />
                    </FormControl>
                    <FormControl variant="filled" disabled className="newDocFormControl">
                        <InputLabel htmlFor="doc.author">Автор</InputLabel>
                        <FilledInput id="doc.author" style={{backgroundColor:'rgb(97 131 175 / 33%)'}} value="Mikhail Prokhorov" />
                    </FormControl>

                    <FormControl variant="filled" disabled className="newDocFormControl">
                        <InputLabel htmlFor="doc.regNumber">Номер проекта</InputLabel>
                        <FilledInput id="doc.regNumber" style={{backgroundColor:'rgb(97 131 175 / 33%)'}} value="№1" />
                    </FormControl>
                </FormGroup>
            </form>
            <hr className="newDocFormHr" />
                
            <h4 className="newDocSectionTitle">Маршрут</h4>
            <form className={classes.root + " newDocForm"} noValidate autoComplete="off">
                <FormGroup className="newDocFormGroupRoute">
                    <FormControl className={classes.formControl + " newDocFormControlRoute"}  style={{backgroundColor:'rgb(97 131 175 / 33%)'}}>
                        <InputLabel id="doc.approvalLabel"><span className="newDocFormInputLabel">Согласование</span></InputLabel>
                        <Select
                        labelId="doc.approvalLabel"
                        id="doc.approval"
                        multiple
                        value={approvalsName}
                        onChange={handleApprovalsChange}
                        input={<Input id="select-multiple-approvals" />}
                        renderValue={(selected) => (
                            <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip}  style={{backgroundColor:'rgb(97 131 175 / 33%)'}}/>
                            ))}
                            </div>
                        )}
                        MenuProps={MenuProps}
                        >
                        {names.map((name) => (
                            <MenuItem key={name} value={name} style={getStyles(name, approvalsName, theme)}>
                            {name}
                            </MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl + " newDocFormControlRoute"}  style={{backgroundColor:'rgb(97 131 175 / 33%)'}}>
                        <InputLabel id="doc.signerLabel"><span className="newDocFormInputLabel">Подписание</span></InputLabel>
                        <Select
                        labelId="doc.signerLabel"
                        id="doc.signer"
                        multiple
                        value={signersName}
                        onChange={handleSignersChange}
                        input={<Input id="select-multiple-signers" />}
                        renderValue={(selected) => (
                            <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip}  style={{backgroundColor:'rgb(97 131 175 / 33%)'}}/>
                            ))}
                            </div>
                        )}
                        MenuProps={MenuProps}
                        >
                        {names.map((name) => (
                            <MenuItem key={name} value={name} style={getStyles(name, signersName, theme)}>
                            {name}
                            </MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl + " newDocFormControlRoute"}  style={{backgroundColor:'rgb(97 131 175 / 33%)'}}>
                        <InputLabel id="doc.receiverLabel"><span className="newDocFormInputLabel">Получатель</span></InputLabel>
                        <Select
                        labelId="doc.receiverLabel"
                        id="doc.receiver"
                        multiple
                        value={receiversName}
                        onChange={handleReceiversChange}
                        input={<Input id="select-multiple-receivers" />}
                        renderValue={(selected) => (
                            <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip}  style={{backgroundColor:'rgb(97 131 175 / 33%)'}}/>
                            ))}
                            </div>
                        )}
                        MenuProps={MenuProps}
                        >
                        {names.map((name) => (
                            <MenuItem key={name} value={name} style={getStyles(name, receiversName, theme)}>
                            {name}
                            </MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                </FormGroup>
            </form>
            <hr className="newDocFormHr" />


            <form className={classes.root + " newDocForm"} noValidate autoComplete="off">
                <FormGroup className="newDocFormGroup">
                    <FormControl variant="filled" className="newDocFormControl">
                        <InputLabel htmlFor="component-filled">Тема</InputLabel>
                        <FilledInput id="component-filled" style={{backgroundColor:'rgb(97 131 175 / 33%)'}} defaultValue="" />
                    </FormControl>
                </FormGroup>
                <FormGroup className="newDocFormGroup">
                    <FormControl variant="filled" className="newDocFormControl">
                            <TextField
                            id="doc.subjectRu"
                            label="Текст"
                            multiline
                            rows={21}
                            defaultValue=""
                            variant="filled"
                            style={{backgroundColor:'rgb(97 131 175 / 33%)'}}
                            />
                    </FormControl>
                </FormGroup>
            </form>
            <hr className="newDocFormHr" />
        </div>
    )
}
