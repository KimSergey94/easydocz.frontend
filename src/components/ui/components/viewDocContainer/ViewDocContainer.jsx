import './viewDocContainer.css'
import AttachFileTwoToneIcon from '@material-ui/icons/AttachFileTwoTone';


export default function ViewDocContainer({doc}) {
    return (
        <div className="viewDocContainer">
            <div className="viewDocContentContainer">
                <div className="docContentContainer">
                    <div className="docContentField">
                        <div style={{float:'left'}} className="textColorBlue">Тема: </div>
                        <div>{doc.doc.title}</div>
                    </div>
                    <div className="docContentField">
                        <div style={{float:'left'}} className="textColorBlue">Текст: </div>
                        <div>{doc.doc.subjectRu}</div>
                    </div>
                    <br/>
                    <br/>
                    <span className="textColorBlue"><AttachFileTwoToneIcon className="attchmentsIcon"/>Приложения</span><br/>
                    <span>Нет вложений</span>
                </div>
                <div className="viewDocInfoRight" style={{borderLeft: '3px orange solid'}}>
                    <div>
                        <span>Автор</span><br />
                        <span>doc.Author</span><br />
                        <span>Филиал</span><br />
                        <span>Author.Филиал</span><br />
                        <span>Департамент</span><br />
                        <span>doc.Author.DepartmentId</span><br />
                        <span>Почта</span><br />
                        <span>sksss060213@gmail.com</span><br />
                        <span>Телефон</span><br />
                        <span>doc.AuthorPhone</span>
                    </div>
                    <div>
                        <div>
                            <span>Создан</span><br />
                            <span>doc.Created</span>
                        </div>
                    </div>
                    <hr />
                    <div>
                        <div>
                            <span>Согласование</span><br />
                            <div>
                                <span>a.Name</span>
                                <span>a.Name2</span>
                                <span>a.Name3</span>
                            </div>
                        </div>
                    </div>
                    <hr />

                </div>
            </div>
        </div>
    )
}
