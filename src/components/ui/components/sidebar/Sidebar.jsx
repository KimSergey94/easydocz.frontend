import './sidebar.css'
import { LineStyle } from '@material-ui/icons'
import { Link } from 'react-router-dom'

export default function Sidebar() {
    return (
        <div className="sidebar"> 
            <div className="sidebarWrapper">
                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">Тестовые страницы</h3>
                    <ul className="sidebarList">
                        <Link to="/dashboard" className="link">
                            <li className="sidebarListItem">
                                <LineStyle className="sidebarIcon"/>
                                Активные задачи
                            </li>
                        </Link>
                        <Link to="/users" className="link">
                            <li className="sidebarListItem">
                                <LineStyle className="sidebarIcon"/>
                                Users
                            </li>
                        </Link>
                        <Link to="/products" className="link">
                            <li className="sidebarListItem">
                                <LineStyle className="sidebarIcon"/>
                                Products
                            </li>
                        </Link>
                    </ul>

                    <h3 className="sidebarTitle">Мои документы</h3>
                    <ul className="sidebarList">
                        <Link to="/mydocs" className="link">
                            <li className="sidebarListItem">
                                <LineStyle className="sidebarIcon"/>
                                Мои документы
                            </li>
                        </Link>
                        <Link to="/mydrafts" className="link">
                            <li className="sidebarListItem">
                                <LineStyle className="sidebarIcon"/>
                                Мои черновики
                            </li>
                        </Link>
                    </ul>
                </div>
            </div>
        </div>
    )
}
