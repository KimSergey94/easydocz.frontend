import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import {Link} from 'react-router-dom'
import Button from '@material-ui/core/Button';


const useRowStyles = makeStyles({
  root: {
    '& > *': {
        color:'#31708f',
        backgroundColor:'rgb(251,251,255)',
        borderBottom: 'unset',
    }
  },
  headTr:{
      '& > *':{
        backgroundColor:'rgb(251,251,255)',
        color:'grey'
      }
  }
});

function createData(id, signer, title, author, documentType, regNumber, created, 
    regData, status, tasks) {
  return {
    id,
    signer,
    title,
    author,
    documentType,
    regNumber,
    created,
    regData,
    status,
    tasks: tasks,
  };
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">{row.signer}</TableCell>
        <Link to="/viewdoc/1" style={{ textDecoration: 'none' }}>
          <TableCell style={{ color:'#31708f', borderBottom:'0' }}>{row.title}</TableCell>
        </Link>
                            {/* <TableCell>{row.title}</TableCell> */}
        <TableCell>{row.author}</TableCell>
        <TableCell>{row.documentType}</TableCell>
        <TableCell>{row.regNumber}</TableCell>
        <TableCell>{row.created}</TableCell>
        <TableCell>{row.regData}</TableCell>
        <TableCell>{row.status}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, backgroundColor:'rgb(251,251,255)' }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1} >
              <Typography variant="h6" gutterBottom component="div">Задачи</Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow className={classes.tr}>
                    <TableCell><b>Тип</b></TableCell>
                    <TableCell><b>Статус</b></TableCell>
                    <TableCell><b>Пользователь</b></TableCell>
                    <TableCell><b>Начало</b></TableCell>
                    <TableCell><b>Конец</b></TableCell>
                    <TableCell><b>Контроль</b></TableCell>
                    <TableCell><b>Комментарий</b></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.tasks.map((taskRow) => (
                    <TableRow key={taskRow.taskId}>
                      <TableCell component="th" scope="row">
                        {taskRow.taskType}
                      </TableCell>
                      <TableCell>{taskRow.status}</TableCell>
                      <TableCell>{taskRow.user}</TableCell>
                      <TableCell>{taskRow.startTime}</TableCell>
                      <TableCell>{taskRow.endTime}</TableCell>
                      <TableCell>{taskRow.control}</TableCell>
                      <TableCell>{taskRow.comment}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    id: PropTypes.number.isRequired,
    signer: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    documentType: PropTypes.string.isRequired,
    regNumber: PropTypes.string.isRequired,
    created: PropTypes.string.isRequired,
    regData: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    tasks: PropTypes.arrayOf(
      PropTypes.shape({
        taskId: PropTypes.number.isRequired,
        taskType: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired,
        startTime: PropTypes.string.isRequired,
        endTime: PropTypes.string.isRequired,
        control: PropTypes.string.isRequired,
        comment: PropTypes.string.isRequired,
      }),
    ).isRequired,
  }).isRequired,
};

const rows = [
  createData(1, '','Уважаемый Айдар Ахметкалиевич!', 'Ким Сергей Дмитриевич', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 1, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 2, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
  createData(2, '','Test3', 'Айдар Ахметкалиевич', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 3, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 4, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
  createData(3, '','Test2!', 'Mikhail Prohorov', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 5, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 6, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
  createData(4, '','Test1', 'Mikhail Prohorov', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 7, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 8, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
  createData(5, '','сз на оплату', 'Mikhail Prohorov', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 9, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 10, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
  createData(6, '','по поводу ...', 'Mikhail Prohorov', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 11, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 12, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
  createData(7, '','отчет за декабрь', 'Mikhail Prohorov', 'Служебная записка', 'ПР-154-36', '22.06.21', '','На подписании', [
    { taskId: 13, taskType: 'Approval', status: 'Ended', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'25.06.2021 12:15', control:'30.06.2021 11:14', comment:'' },
    { taskId: 14, taskType: 'Signer', status: 'In work', user: 'Mikhail Prohorov', startTime:'23.06.2021 11:14', endTime:'', control:'30.06.2021 11:14', comment:'' },
  ]),
];

export default function MyDocsContainer() {
  const classes = useRowStyles();
  return (
    <div className="myDocsContainer">
        <TableContainer component={Paper} className={classes.root + " tableContainer"}>
            <Table aria-label="collapsible table">
                <TableHead>
                <TableRow className={classes.headTr}>
                    <TableCell />
                    <TableCell>Подписант</TableCell>
                    <TableCell>Тема</TableCell>
                    <TableCell>Автор</TableCell>
                    <TableCell>Тип документа</TableCell>
                    <TableCell>Номер</TableCell>
                    <TableCell>Создан</TableCell>
                    <TableCell>Рег. дата</TableCell>
                    <TableCell>Статус</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((row) => (
                    <Row key={row.id} row={row} />
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    </div>

  );
}