import {Switch, Route, Redirect} from 'react-router-dom';
import React, {Component} from 'react';
import LoadPage from './pages/PageLoader';
import {docs} from '../shared/testdocs';
import { TAB_TYPE } from '../shared/TabType';
import Topbar from './components/topbar/Topbar';
import Sidebar from './components/sidebar/Sidebar';

class Main extends Component {
  constructor(props){
    super(props);

    this.state = {
       docs: docs
    };
  }

  render(){
    // const DashboardPage = () => {
    //   return (
    //     <LoadPage docs={this.state.docs} tabType={TAB_TYPE.DASHBOARD}></LoadPage>
    //   );
    // }
    
    return (
      <div>
        <Topbar />
        <div className="container">
          <Sidebar />
          <Switch>
            
         
            <Route exact path="/users" component={() => <LoadPage tabType={TAB_TYPE.USERS} withMargin></LoadPage>}></Route>
            <Route path="/user/:userId" component={() => <LoadPage tabType={TAB_TYPE.USER}></LoadPage>}></Route>
            <Route exact path="/newuser" component={() => <LoadPage tabType={TAB_TYPE.NEW_USER}></LoadPage>}></Route>
            <Route exact path="/products" component={() => <LoadPage tabType={TAB_TYPE.PRODUCTS} withMargin></LoadPage>}></Route>
            <Route path="/product/:productId" component={() => <LoadPage tabType={TAB_TYPE.PRODUCT}></LoadPage>}></Route>
            <Route exact path="/newproduct" component={() => <LoadPage tabType={TAB_TYPE.NEW_PRODUCT}></LoadPage>}></Route>
           
            <Route path="/dashboard" component={() => <LoadPage docs={this.state.docs} tabType={TAB_TYPE.DASHBOARD}></LoadPage>}></Route>
            <Route exact path="/mydrafts" component={() => <LoadPage docs={this.state.docs} tabType={TAB_TYPE.MY_DRAFTS}></LoadPage>}></Route>
            <Route exact path="/mydocs" component={() => <LoadPage docs={this.state.docs} tabType={TAB_TYPE.MY_DOCS}></LoadPage>}></Route>
            <Route exact path="/newDoc" component={() => <LoadPage tabType={TAB_TYPE.NEW_DOC}></LoadPage>}></Route>
            {/* docId={docId} */}
            <Route path="/viewDoc/:docId" component={(props) => <LoadPage docs={this.state.docs} tabType={TAB_TYPE.VIEW_DOC} {...props}></LoadPage>}></Route>
            <Route path="/editDoc/:docId" component={() => <LoadPage tabType={TAB_TYPE.EDIT_DOC}></LoadPage>}></Route>

            <Redirect to="/dashboard"></Redirect>
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main;