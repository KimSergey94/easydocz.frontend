import './product.css'
import {productData} from '../../../shared/productsData'
import {Publish} from '@material-ui/icons'
import {Link} from 'react-router-dom'
import Chart from '../../components/chart/Chart'

export default function Product() {
    return (
        <div className="product">
            <div className="productTitleContainer">
                <h1 className="productTitle">Product</h1>
                <Link to="/newproduct">
                    <button className="productAddButton">Create</button>
                </Link>
            </div>
            <div className="productTop">
                <div className="productTopLeft">
                    <Chart data={productData} dataKey="Sales" title="Sales Performance" />
                </div>
                <div className="productTopRight">
                    <div className="productInfoTop">
                        <img src="" alt="" className="ProductInfoImg" />
                        <span className="productName">Apple2</span>
                    </div>
                    <div className="productInfoBottom">
                        <div className="ProductInfoItem">
                            <span className="ProductInfoKey">id:</span>
                            <span className="ProductInfoValue">123</span>
                        </div>
                        <div className="ProductInfoItem">
                            <span className="ProductInfoKey">sales:</span>
                            <span className="ProductInfoValue">5123</span>
                        </div>
                        <div className="ProductInfoItem">
                            <span className="ProductInfoKey">active:</span>
                            <span className="ProductInfoValue">yes</span>
                        </div>
                        <div className="ProductInfoItem">
                            <span className="ProductInfoKey">in stock:</span>
                            <span className="ProductInfoValue">no</span>
                        </div>
                        <div className="ProductInfoItem">
                            <span className="ProductInfoKey">id:</span>
                            <span className="ProductInfoValue">123</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="productBottom">
                <form className="productForm">
                    <div className="productFormLeft">
                        <label>Product Name</label>
                        <input type="text" placeholder="Apple2" />
                        <label>In Stock</label>
                        <select name="inStock" id="idStock">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                        <label>Active</label>
                        <select name="active" id="active">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                    <div className="productFormRight">
                        <div className="productUpload">
                            <img src="" alt="" className="productUploadImg" />
                            <label htmlFor="file">
                                <Publish />
                            </label>
                            <input type="file" style={{display:"none"}} />
                        </div>
                        <button className="productButton">Update</button>
                    </div>
                </form>
            </div>
        </div>
    )
}
