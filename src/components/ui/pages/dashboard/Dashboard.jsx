import React from 'react'
import './dashboard.css'
import FeaturedInfo from '../../components/featuredInfo/FeaturedInfo'
import Chart from '../../components/chart/Chart'
import {chartData} from '../../../shared/chartData'
import WidgetSm from '../../components/widgetSm/widgetSm'
import WidgetLg from '../../components/widgetLg/widgetLg'

export default function Dashboard() {
    return (
        <div className="dashboard">
            <FeaturedInfo />
            <Chart data={chartData} title="User Analytics" grid dataKey="Active user" />
            <div className="dashboardWidgets">
                <WidgetSm />
                <WidgetLg />
            </div>
        </div>
    )
}