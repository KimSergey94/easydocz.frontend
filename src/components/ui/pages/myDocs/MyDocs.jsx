import './myDocs.css'
import React from 'react'
// import { makeStyles } from '@material-ui/core/styles'
import CreateButtonStyle from "../../components/CreateButtonStyle"
import Button from '@material-ui/core/Button'
import FindInPageTwoToneIcon from '@material-ui/icons/FindInPageTwoTone'
import MyDocsContainer from "../../components/myDocsContainer/MyDocsContainer"

    // const useStyles = makeStyles((theme) => ({
    //     button1: {
    //         margin: theme.spacing(1),
    //         color: 'yellow',
    //         backgroundColor: 'black',
    //     },
    //     button2: {
    //         margin: theme.spacing(1),
    //         color: 'red',
    //         backgroundColor: 'green'
    //     }
    // }));

export default function MyDocs() {
    // const classes = useStyles();

    return (
        <div className="myDocs">
            <div className="contentHeader">
                <h2 className="myDocsTitle">Мои документы</h2>
                <div className="docFilters">
                    <CreateButtonStyle label="Все"
                        color="white"
                        backgroundColor="rgb(76, 174, 76)"
                        hoverBackgroundColor="#4C7951"
                        onClick={() => {console.log("clicked 1");}}
                        // className={classes.test}
                    >
                    </CreateButtonStyle>
                    <CreateButtonStyle label="Входящий документ"
                        color="white"
                        backgroundColor="rgb(174, 76, 105)"
                        hoverBackgroundColor="#6C1915"
                        onClick={() => {console.log("clicked 2");}}>
                    </CreateButtonStyle>
                    <CreateButtonStyle label="Исходящий документ"
                        color="white"
                        backgroundColor="rgb(76, 133, 174)"
                        hoverBackgroundColor="#153F6C"
                        onClick={() => {console.log("clicked 3");}}>
                    </CreateButtonStyle>
                </div>
                <div className="searchPanel">
                    <div className="searchPanelButtonContainer">
                        <Button color="primary">
                            <FindInPageTwoToneIcon />
                            Поиск
                        </Button>
                    </div>
                    <div className="clearFix"></div>
                    <div className="filter-body"></div>
                </div>
            </div>
            <div className="pageViewContainer">
                <MyDocsContainer/>
            </div>
        </div>
    )
}