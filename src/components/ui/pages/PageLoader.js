import React from 'react';
import { TAB_TYPE } from '../../shared/TabType';
import Dashboard from './dashboard/Dashboard';
import UserList from './userList/UserList';
import User from './user/User'
import NewUser from './newUser/NewUser'
import ProductList from './productList/ProductList';
import Product from './product/Product';
import NewProduct from './newProduct/NewProduct';
import MyDocs from './myDocs/MyDocs';
import NewDoc from './newDoc/NewDoc';
import ViewDoc from './viewDoc/ViewDoc';


function RenderMyDocsPage({ docs, onClick }){
    return <MyDocs/>
    // return docs.map((doc) => {
    //     return (
    //         <div key={doc.id} className="col-12 col-md-5 col-lg-5 col-xl-5 m-1">
    //             <span key={doc.id}> RenderMyDocs {doc.id}: {doc.name}</span>
    //         </div>
    //     );
    // });
}
function RenderNewDocPage({docs}){
    return <NewDoc docs={docs}/>
}
function RenderViewDocPage(doc){
    return <ViewDoc doc={doc}/>
}
function RenderEditDocPage({doc}){
    return <ViewDoc doc={doc}/>
}

function RenderMyDraftsPage({docs}){
    return (
        <div key={docs.id} style={{flex:4}}>
            <span>drafts</span>
        </div>
        );
}
function InitTab(props){
    if(props.tabType === TAB_TYPE.USERS){
        return <UserList />
    } else if(props.tabType === TAB_TYPE.USER){
        return <User />
    } else if(props.tabType === TAB_TYPE.NEW_USER){
        return <NewUser />
    } else if(props.tabType === TAB_TYPE.PRODUCTS){
        return <ProductList />
    } else if(props.tabType === TAB_TYPE.PRODUCT){
        return <Product />
    } else if(props.tabType === TAB_TYPE.NEW_PRODUCT){
        return <NewProduct />
    } 
    
    else if(props.tabType === TAB_TYPE.MY_DRAFTS){
        // return props.docs.map((doc) => {
            return <RenderMyDraftsPage docs={props.docs} ></RenderMyDraftsPage>
        // });
    } else if(props.tabType === TAB_TYPE.DASHBOARD){
        return <Dashboard />
    } else if(props.tabType === TAB_TYPE.MY_DOCS){
        return <RenderMyDocsPage docs={props.docs} ></RenderMyDocsPage>
    } 
    
    else if(props.tabType === TAB_TYPE.NEW_DOC){
        return <RenderNewDocPage docs={props.docs} ></RenderNewDocPage>
    } else if(props.tabType === TAB_TYPE.VIEW_DOC){
        let doc = props.docs.filter((doc)=> doc.id == props.match.params.docId);
        return <RenderViewDocPage doc={doc ? doc[0] : {}}></RenderViewDocPage>
    } else if(props.tabType === TAB_TYPE.EDIT_DOC){
        return <RenderEditDocPage docs={props.docs} ></RenderEditDocPage>
    }

    return <span>default InitTab</span>
}


const LoadPage = (props) => {
    const pageView = InitTab(props);
    
    return (
        <div className="pageView"> 
            {pageView}
        </div>
    );
}

export default LoadPage;