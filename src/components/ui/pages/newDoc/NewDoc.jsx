import './newDoc.css'
import React from 'react'
import Button from '@material-ui/core/Button'
import NewDocContainer from "../../components/newDocContainer/NewDocContainer"
import SaveTwoToneIcon from '@material-ui/icons/SaveTwoTone';
import PrintTwoToneIcon from '@material-ui/icons/PrintTwoTone';
import SendTwoToneIcon from '@material-ui/icons/SendTwoTone';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import ArrowBackTwoToneIcon from '@material-ui/icons/ArrowBackTwoTone';

export default function NewDoc() {
    return (
        <div className="newDoc">
            <div className="contentHeader">
                <div className="newDocTitle">
                        <h2 className="newDocTitleText">Служебная записка</h2>
                </div>
                <div className="toolPanel">
                    <div className="toolPanelButtonContainer">
                        <Button color="primary">
                            <SaveTwoToneIcon />
                            Сохранить
                        </Button>
                        <Button color="primary">
                            <PrintTwoToneIcon />
                            Версия для печати
                        </Button>
                        <Button color="primary">
                            <SendTwoToneIcon />
                            Отправить
                        </Button>
                        <Button color="primary">
                            <DeleteTwoToneIcon />
                            Удалить
                        </Button>
                        <Button color="primary">
                            <ArrowBackTwoToneIcon />
                            Назад
                        </Button>
                    </div>
                </div>
            </div>
            <div className="pageViewContainer">
                <NewDocContainer/>
            </div>
        </div>
    )
}
