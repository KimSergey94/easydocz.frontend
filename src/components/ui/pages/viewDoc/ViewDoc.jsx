import './viewDoc.css'
import Button from '@material-ui/core/Button'
import ViewDocContainer from "../../components/viewDocContainer/ViewDocContainer"
import PrintTwoToneIcon from '@material-ui/icons/PrintTwoTone';
import ArrowBackTwoToneIcon from '@material-ui/icons/ArrowBackTwoTone';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';
import DoneAllTwoToneIcon from '@material-ui/icons/DoneAllTwoTone';
import FeedbackTwoToneIcon from '@material-ui/icons/FeedbackTwoTone';
import GroupAddTwoToneIcon from '@material-ui/icons/GroupAddTwoTone';
import BookmarksTwoToneIcon from '@material-ui/icons/BookmarksTwoTone';
import DeveloperBoardTwoToneIcon from '@material-ui/icons/DeveloperBoardTwoTone';
import ControlCameraTwoToneIcon from '@material-ui/icons/ControlCameraTwoTone';
import CancelPresentationTwoToneIcon from '@material-ui/icons/CancelPresentationTwoTone';
import SendTwoToneIcon from '@material-ui/icons/SendTwoTone';

export default function ViewDoc({doc}) {
    return (
        <div className="viewDoc">
            

        <div className="contentHeader">

            <div className="toolPanel">
                <div className="viewDocTitle"><h2 className="viewDocTitleText">Служебная записка</h2></div>
                
                <div className="toolPanelButtonContainer">
                    <Button color="primary">
                        <BookmarksTwoToneIcon />
                        Избранное
                    </Button>
                    <Button color="primary">
                        <SendTwoToneIcon />
                        Разослать документ
                    </Button>
                    <Button color="primary">
                        <CancelPresentationTwoToneIcon />
                        Отменить документ
                    </Button>
                    <Button color="primary">
                        <ControlCameraTwoToneIcon />
                        Контроль
                    </Button>
                    <Button color="primary">
                        <EditTwoToneIcon />
                        Редактировать
                    </Button>
                    <Button color="primary">
                        <PrintTwoToneIcon />
                        Версия для печати
                    </Button>
                    <Button color="primary">
                        <DeveloperBoardTwoToneIcon />
                        Использовать как шаблон
                    </Button>
                    <Button color="primary">
                        <DoneAllTwoToneIcon />
                        Прочтено
                    </Button>
                    <Button color="primary">
                        <GroupAddTwoToneIcon />
                        Добавить согласование
                    </Button>
                    <Button color="primary">
                        <FeedbackTwoToneIcon     />
                        Для сведения/Ознакомить
                    </Button>
                    <Button color="primary">
                        <ArrowBackTwoToneIcon />
                        Назад
                    </Button>
                </div>
            </div>
        </div>
        <div className="pageViewContainer">
            <ViewDocContainer doc={doc}/>
        </div>
    </div>
    )
}
