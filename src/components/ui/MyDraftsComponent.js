import React from 'react';
import {Card, CardImg, CardImgOverlay, CardTitle } from 'reactstrap';

function RenderMenuItem({ dish, onClick }){
    return (
        <Card onClick={() => onClick(dish.id)} >
            <CardImg width="100%" src={dish.image} alt={dish.name}></CardImg>
            <CardImgOverlay>
                <CardTitle>{dish.name}</CardTitle>
                <p>{dish.description}</p>
            </CardImgOverlay>
        </Card>
    );
}

const Menu = (props) => {
    const menu = props.docs.map((doc) => {
        return (
            <div key={doc.id} className="col-12 col-md-5 col-lg-5 col-xl-5 m-1">
                <RenderMenuItem dish={doc} onClick={props.onClick}></RenderMenuItem>
            </div>
        );
    })
    
    return (
        <div className="container">
            <div className="row">
                {menu}
            </div>
        </div>
    );
}

export default Menu;