import React, {Component} from 'react';
import { Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem, Jumbotron } from 'reactstrap';
import { NavLink } from 'react-router-dom'; 

class Header extends Component {

    constructor(props){
        super(props);

        this.state = {
            isNavOpen: false
        }
        this.toggleNav = this.toggleNav.bind(this);
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }

    render() {
        return(
            <div style={{flex:4}}>
                <Navbar dark expand="md">
                    <div className="">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='Ristorante Con Fusion' /></NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link"  to='/dashboard'><span className="fa fa-home fa-lg"></span>Активные задачи</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/mydrafts'><span className="fa fa-list fa-lg"></span>Мои черновики</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/mydocs'><span className="fa fa-address-card fa-lg"></span>Мои документы</NavLink>
                            </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className="">
                        <div className="row row-header">
                            <div className="col-12 col-sm-6">
                                <h1>Ristorante con Fusion</h1>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>a
            </div>
        );
    }
}

export default Header;